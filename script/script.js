// Опишіть своїми словами що таке Document Object Model (DOM)
// DOM це подання HTML-документа із всіма вкладеними в нбого тегами у вигляді дерева.

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// Властивість innerHTML отримує весь контент разом із тегами, що вказані на HTML-сторінці для цього елемента.
// innerText робить те саме але без вкладених тегів, тому що ця властивість їх екранує і на виході вона відображаються як звичайний текст.

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// getElementById, querySelector, querySelectorAll, getElementsByClassName, getElementsByTagName. На мою думку ліпше використувувати перші 3, в залежності від завдання.

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.querySelectorAll('p');

paragraphs.forEach(paragraph => {
    paragraph.style.backgroundColor = '#ff0000';
});

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const parentElement = optionsList.parentElement;
console.log(parentElement);

optionsList.childNodes.forEach(childNode => {
    console.log(`Назва: ${childNode.nodeName}, тип: ${childNode.nodeType}`);
});

// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
const testParagraph = document.querySelector('.testParagraph');
testParagraph.textContent = 'This is a paragraph';

// Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector('.main-header');
const nestedElements = mainHeader.querySelectorAll('*');
nestedElements.forEach(element => {
    console.log(nestedElements);
    element.classList.add('nav-item');
});

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTittles = document.querySelectorAll('.section-title');
sectionTittles.forEach(title => {
    title.classList.remove('section-title');
});